#include "catch.hpp"
#include "felix2atlas/SubscribeClient.hpp"

#include <nlohmann/json.hpp>

std::string valid = "{\"hostname\":\"pc-tbed-felix-07\",\"device\":\"3\",\"global\":{\"throughput[Gbps]\":0.20,\"chunkrate[kHz]\":967}}\n";
std::string valid_out = "{\"hostname\":\"pc-tbed-felix-07\",\"device\":\"3\",\"global\":{\"throughput[Gbps]\":0.20,\"chunkrate[kHz]\":967}}";

std::string partial_1= "{\"hostname\":\"pc-tbed-felix-07\",\"device\":\"3\",\"glo";
std::string partial_2= "bal\":{\"throughput[Gbps]\":0.20,\"chunkrate[kHz]\":967}}\n";

TEST_CASE( "Testing message parsing", "[parsing]" ) {

    auto sc = daq::felix2atlas::SubscribeClient();
    std::string buffer{};

    SECTION( "Case 1: walid whole message" ) {
        std::vector<std::string> output = sc.tokenize(valid, buffer);
        REQUIRE( output.size() == 1 );
        REQUIRE( buffer.size() == 0 );
        REQUIRE( output.at(0) == valid_out );
    }

    SECTION( "Case 2: partial read (message split in two parts)" ) {
        std::vector<std::string> first = sc.tokenize(partial_1, buffer);
        std::vector<std::string> second = sc.tokenize(partial_2, buffer);
        REQUIRE( first.size() == 0 );
        REQUIRE( second.size() == 1 );
        REQUIRE( second.at(0) == valid_out );
    }

    SECTION( "Case 3: whole plus partial read (message split in two parts)" ) {
        std::string one_and_a_half = valid+partial_1;
        std::vector<std::string> first = sc.tokenize(one_and_a_half, buffer);
        std::vector<std::string> second = sc.tokenize(partial_2, buffer);
        REQUIRE( first.size() == 1 );
        REQUIRE( second.size() == 1 );
        REQUIRE( first.at(0) == valid_out );
        REQUIRE( second.at(0) == valid_out );
    }

    SECTION( "Case 4: invalid plus valid" ) {
        std::vector<std::string> first = sc.tokenize(partial_2, buffer);
        std::vector<std::string> second = sc.tokenize(valid, buffer);
        REQUIRE( first.size() == 1 );
        REQUIRE( nlohmann::json::accept(first.at(0)) == false );
        REQUIRE( second.size() == 1 );
        REQUIRE( second.at(0) == valid_out );
    }

    SECTION( "Case 5: message split in three parts" ) {
        std::string p1 = "{\"hostname\":\"pc-tbed-felix-07\",\"de";
        std::string p2 = "vice\":\"3\",\"glo";
        std::string p3 = "bal\":{\"throughput[Gbps]\":0.20,\"chunkrate[kHz]\":967}}\n";
        std::vector<std::string> first = sc.tokenize(p1, buffer);
        std::vector<std::string> second = sc.tokenize(p2, buffer);
        std::vector<std::string> third = sc.tokenize(p3, buffer);
        REQUIRE( first.size() == 0 );
        REQUIRE( second.size() == 0 );
        REQUIRE( third.size() == 1 );
        REQUIRE( third.at(0) == valid_out );
    }

    SECTION( "Case 6: valid plus one third, then other two pieces, then valid" ) {
        std::string valid_plus_p1 = valid + "{\"hostname\":\"pc-tbed-felix-07\",\"de";
        std::string p2 = "vice\":\"3\",\"glo";
        std::string p3 = "bal\":{\"throughput[Gbps]\":0.20,\"chunkrate[kHz]\":967}}\n";
        std::vector<std::string> first = sc.tokenize(valid_plus_p1, buffer);
        std::vector<std::string> second = sc.tokenize(p2, buffer);
        std::vector<std::string> third = sc.tokenize(p3, buffer);
        std::vector<std::string> fourth = sc.tokenize(valid, buffer);
        REQUIRE( first.size() == 1 );
        REQUIRE( first.at(0) == valid_out );
        REQUIRE( second.size() == 0 );
        REQUIRE( third.size() == 1 );
        REQUIRE( third.at(0) == valid_out );
        REQUIRE( fourth.size() == 1 );
        REQUIRE( fourth.at(0) == valid_out );
    }

}
