#include "catch.hpp"
#include "felix2atlas/IsSink.hpp"

#include <nlohmann/json.hpp>
#include <fstream>

TEST_CASE( "Global tohost stats", "[content]" ) {

    std::ifstream ifs("tohost_sample.json");
    nlohmann::json root =  nlohmann::json::parse(ifs);

    daq::felix2atlas::IsSink sink{"initial", "FELIX", 0};
    sink.init({});
    daq::felix2atlas::FELIX_globalNamed global ("initial", "host.d0.tohost.global");

    for (auto itr = root.begin(); itr != root.end(); itr++) {
        if (itr.key() == "hostname"){global.hostname =  (*itr).get<std::string>();}
        else if (itr.key() == "device"){global.device = (*itr).get<std::string>();}
        else if (itr.key() == "global"){sink.processGlobalStats(*itr, &global);}
        else{ continue; }
    }

    REQUIRE(global.hostname == "pc-tbed-felix-07"); 
    REQUIRE(global.device == "0");
    REQUIRE(global.daq_pages == 256);
    REQUIRE(global.dcs_pages == 512);
    REQUIRE(global.ttc_pages == 0);
    REQUIRE(global.daq_co == 0);
    REQUIRE(global.dcs_co == 0);
    REQUIRE(global.ttc_co == 127);
    REQUIRE(global.buffer_events == 0);
    REQUIRE(global.polls == 0);
    REQUIRE(global.available_buf == 965.617188);
    REQUIRE(global.busy == 0);
    REQUIRE(global.xl1id == 23417891);
    REQUIRE(global.ecrs_from_ttc == 1);
    REQUIRE(global.ttype_from_ttc == 0);
    REQUIRE(global.bcr_mismatch == 791735536);
    REQUIRE(global.bcr == 0);
}
