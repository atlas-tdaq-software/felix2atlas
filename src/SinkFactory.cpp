#include "felix2atlas/SinkFactory.hpp"
#include "felix2atlas/ErsSink.hpp"
#include "felix2atlas/IsSink.hpp"
#include "felix2atlas/FelixERSreceiver.hpp"

constexpr static const char IS_PARTITION_LABEL[] = "partition";
constexpr static const char IS_SERVER_LABEL[] = "server";
constexpr static const char IS_MODE_LABEL[] = "mode";

daq::felix2atlas::Sink *daq::felix2atlas::SinkFactory::createSink(const daq::felix2atlas::Configuration::ChannelConfig::SinkConfig &sinkConfig) {
    if (sinkConfig.mSinkType == "ers") return new daq::felix2atlas::ErsSink();
    if (sinkConfig.mSinkType == "is") {
        bool _ISMode;
        if(sinkConfig.mParams.at(IS_MODE_LABEL) == "default") _ISMode = false;
        else if(sinkConfig.mParams.at(IS_MODE_LABEL) == "history") _ISMode = true;
        else throw felix2atlas::Felix2atlasIssue(ERS_HERE, "Invalid IS mode in config: " + sinkConfig.mParams.at(IS_MODE_LABEL));

        return new daq::felix2atlas::IsSink(sinkConfig.mParams.at(IS_PARTITION_LABEL),
                               sinkConfig.mParams.at(IS_SERVER_LABEL),
                               _ISMode);
    }
    throw felix2atlas::Felix2atlasIssue(ERS_HERE, "Unknown sink type: " + sinkConfig.mSinkType);
}
