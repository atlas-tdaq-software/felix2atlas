#include <iomanip>
#include <ers/ers.h>
#include <ers/AnyIssue.h>
#include "felix2atlas/FelixERSreceiver.hpp"


::ers::Severity daq::felix2atlas::FelixERSreceiver::severityFromString(const std::string &severityString) {
    if (severityString == "DEBUG") return ::ers::severity::Log;
    if (severityString == "TRACE") return ::ers::severity::Debug;
    ::ers::Severity severity(::ers::severity::Fatal);
    return ::ers::parse(severityString, severity);
}

system_clock::time_point daq::felix2atlas::FelixERSreceiver::timestampFromString(const std::string &timestampString) {
    std::tm cTimestamp{};
    cTimestamp.tm_isdst = -1;
    std::istringstream ss(timestampString);
    ss >> std::get_time(&cTimestamp, "%Y-%m-%dT%H:%M:%S");
    system_clock::time_point timePoint = system_clock::from_time_t(mktime(&cTimestamp));
    uint16_t millisecondsFromString;
    if (sscanf(timestampString.c_str(), "%*u-%*u-%*uT%*u:%*u:%*u.%huZ", &millisecondsFromString) != 1) {
        throw felix2atlas::Felix2atlasIssue(ERS_HERE,"Unable to parse timestamp string from Felix: '" + timestampString + "'");
    }
    return timePoint;
}


void daq::felix2atlas::FelixERSreceiver::sendERS(std::string msgID, const ::ers::RemoteContext &context, const std::string &severity,
                                 const std::string &timestamp, const std::string &message, 
                                 const std::vector<std::string>& qualifiers, const std::map<std::string, std::string>& parameters){

    ers::AnyIssue issue = ers::AnyIssue(msgID, severityFromString(severity), context, timestampFromString(timestamp), message, qualifiers, parameters);
    switch (issue.severity()) {
        case ::ers::severity::Log:
            ::ers::log(issue);
            break;
        case ::ers::Debug:
            ::ers::debug(issue);
            break;
        case ::ers::Information:
            ::ers::info(issue);
            break;
        case ::ers::Warning:
            ::ers::warning(issue);
            break;
        case ::ers::Error:
            ::ers::error(issue);
            break;
        case ::ers::Fatal:
            ::ers::fatal(issue);
            break;
    }
}
