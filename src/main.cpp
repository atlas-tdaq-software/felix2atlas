#include <iostream>
#include <csignal>
#include <boost/program_options.hpp>
#include "felix2atlas/Configuration.hpp"
#include "ers/ers.h"
#include "felix2atlas/SubscribeClient.hpp"



int main (int argc, char** argv)
{
    struct daq::felix2atlas::Config cfg = daq::felix2atlas::Config();
    boost::program_options::options_description desc("felix-to-atlas - Forwarding Error-out to ERS and Stats-out to IS");
    desc.add_options()
    ("help,h", "produce help message")
    ("mode,m", boost::program_options::value<std::string>(&cfg.mode)->default_value("default"), "Mode")
    ("server,s", boost::program_options::value<std::string>(&cfg.server)->default_value("FELIX"), "IS server")
    //("interface,i",  boost::program_options::value<std::string>(&cfg.localHostName), "Local interface")
    ("ers",  boost::program_options::value<std::vector<std::string>>(&cfg.ersFifos), "Error-out fifo")
    ("is",  boost::program_options::value<std::vector<std::string>>(&cfg.isFifos)->multitoken(), "Stats-out fifo")
    ("partition,p",  boost::program_options::value<std::string>(&cfg.partition), "Partition name"); 

    boost::program_options::variables_map vm;
    boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);  

    if (vm.count("help")) {
        std::cout << desc << std::endl;
        return 1;
    }

    daq::felix2atlas::Configuration config(&cfg);
    daq::felix2atlas::IsSink::init(config.getISInitList());

    daq::felix2atlas::SubscribeClientFIFO subclient = daq::felix2atlas::SubscribeClientFIFO();
    subclient.connect(&config);
}