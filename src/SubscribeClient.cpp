#include "felix2atlas/SubscribeClient.hpp"
#include <sys/stat.h>

bool operator<(const pollfd& lfd, const pollfd& rfd) { return lfd.fd < rfd.fd;}
bool operator==(const pollfd& lfd, const pollfd& rfd) { return lfd.fd == rfd.fd;}
    
void daq::felix2atlas::SubscribeClient::on_init() {
    printf("on_init called\n");
}

void daq::felix2atlas::SubscribeClient::on_connect(uint64_t fid) {
    printf("on_connect called 0x%lx\n", fid);
}

void daq::felix2atlas::SubscribeClient::on_disconnect(uint64_t fid) {
    printf("on_disconnect called 0x%lx\n", fid);
}

std::vector<std::string> daq::felix2atlas::SubscribeClient::tokenize(std::string& rawdata, std::string& buffer)
{
    std::istringstream stream(rawdata);
    std::vector<std::string> messages;
    for (std::string msg; std::getline(stream, msg, '\n');){
        msg.erase(std::remove(msg.begin(), msg.end(), '\t'), msg.end());
        messages.push_back(msg);
    }
    //check if there is an incomplete message in the buffer
    //if yes, and message[0] invalid, merge
    if (buffer.size() > 0){
        if ( !nlohmann::json::accept(messages.at(0)) ){
            messages.at(0) = buffer + messages.at(0);
        }
        buffer = ""; 
    }
    //if last message is incomplete save it and do not send it
    if (rawdata.back() != '\n'){
        buffer = messages.at(messages.size()-1);
        messages.pop_back();
    }
    return messages;
}


void daq::felix2atlas::SubscribeClient::on_data(std::string fifo, const uint8_t* data, size_t size) {

    if (sinkByFifo.find(fifo)!= sinkByFifo.end()){

        auto sink = (sinkByFifo.find(fifo)->second);
        std::string rawdata((char*)data, size);
        std::vector<std::string> messages = tokenize(rawdata, sink->BufferedJson);

        for( auto &msg : messages ){
            if ( nlohmann::json::accept(msg) ){
                try {
                    sink->dispatch(msg);
                } catch (std::exception &ex) {
                    ::ers::error(felix2atlas::JsonIssue(ERS_HERE, std::string("Can't dispatch message: ") + ex.what()));
                }
            } else {
                ::ers::debug(felix2atlas::JsonIssue(ERS_HERE,"Invalid json message: " + msg));
            }
        }
    } else {
        ::ers::error(felix2atlas::Felix2atlasIssue(ERS_HERE, "sink not found"));
    }
}

void daq::felix2atlas::SubscribeClientFIFO::read_fifo(const std::vector<std::string>& confs){
    ::ers::debug(felix2atlas::FIFOIssue(ERS_HERE, std::string("Polling on ") + std::to_string(confs.size()) + std::string(" FIFOs")));
    unsigned n, k;
    short revents;
    struct stat buf;
    try {
        for(n = 0; n < confs.size(); n++){
            if (stat(confs[n].c_str(), &buf) != -1)
            {
                int fd = open(confs[n].c_str(), O_RDONLY | O_NONBLOCK);
                if (fd == -1){throw felix2atlas::FIFOIssue(ERS_HERE, std::string("Can't read fifo: ") +  confs[n].c_str());}
                pfds_manager->add_pfd(pollfd{fd, POLLIN, 0}, confs[n].c_str()); 
            } else {
                throw felix2atlas::FIFOIssue(ERS_HERE, std::string("FIFO does not exist: ") + confs[n].c_str());
            }
        }
    } catch (std::exception &ex) {
        ::ers::error(felix2atlas::FIFOIssue(ERS_HERE, "Unable to open FIFO: " + std::string(ex.what())));
    }
    const uint64_t size = pfds_manager->get_pfds().size();
    while(size > 0){
        uint8_t buf[16384] = "";
        std::vector<struct pollfd> current_fds = pfds_manager->get_pfds();
        if (current_fds.size() > 0){
            int i = poll(&current_fds[0], current_fds.size(), 1000);

            if (i == -1){
                int errsv = errno;
                if(errsv != EAGAIN){
                    ::ers::error(felix2atlas::FIFOIssue(ERS_HERE, std::string("Polling error: ") + strerror(errsv)));
                }
                continue;
            }
            for(auto& pfd : current_fds){
                revents = pfd.revents;
                if (revents & POLLIN) {
                    k = read(pfd.fd, buf, sizeof(buf));
                    if( k > 0 ){
                        try{
                            on_data(pfds_manager->get_fifo_by_fd(pfd.fd), buf, k);
                        } catch (std::exception &ex) {
                            ::ers::error(felix2atlas::FIFOIssue(ERS_HERE, "Unable to process read data: " + std::string(ex.what())));
                        }
                    }
                }
                if (revents & POLLHUP) {
                    ::ers::error(felix2atlas::Felix2atlasIssue(ERS_HERE, std::string("Fifo closed now.")));
                    pfds_manager->remove_pfd(pfd);      
                } 
            }
        } else { 
            sleep(1);
        }
    }
    fifos_configured.store(false);
}

void daq::felix2atlas::SubscribeClientFIFO::connect(Configuration* config)
{   
    const std::vector<daq::felix2atlas::Configuration::ChannelConfig> &channelConfigs = config->getChannelConfig();
    std::vector<std::string> confs;
    pfds_manager = std::make_unique<daq::felix2atlas::PfdsManager>();
    for (const auto &channelConfig : channelConfigs) {
        std::string fifo = channelConfig.getFifo();
        auto sink = daq::felix2atlas::SinkFactory::createSink(channelConfig.getSinkConfig());
        sinkByFifo.insert({fifo, sink});
        confs.push_back(fifo);
        // for(auto& is : channelConfig.getSinkConfig().mParams){
        //     ::ers::debug(felix2atlas::Felix2atlasIssue(ERS_HERE, std::string("Sink created for fifo: ") + fifo + std::string(" of type: ") + is.first + " : " + is.second));
        // }
    }
    std::thread closedFifoChecker(&daq::felix2atlas::SubscribeClientFIFO::check_closed_fifos, this);
    read_fifo(confs);
    closedFifoChecker.join();
}

void daq::felix2atlas::SubscribeClientFIFO::check_closed_fifos(){
    while (fifos_configured.load()){
        if(pfds_manager->closed_fifos_available()){
            std::vector<struct pollfd> cfds = pfds_manager->get_closed_fds();
            poll(&cfds[0], cfds.size(), -1);
            for(auto& pfd : cfds){
                short revents = pfd.revents;
                if (revents & POLLHUP) {
                    continue;
                }
                pfds_manager->add_pfd(pfd);
            }
        }
        sleep(1);
    }
}

void daq::felix2atlas::PfdsManager::remove_pfd(pollfd pfd){
    std::unique_lock<std::mutex> lock(pfd_mtx);
    auto it = std::find(pfds.begin(),pfds.end(),pfd);
    if(it != pfds.end()){
        pfds.erase(it);
        closed_pfds.insert(pfd);
    } else {
        throw felix2atlas::FIFOIssue(ERS_HERE, std::string("Polled FD not in plfds, cannot be removed."));
    }
}

void daq::felix2atlas::PfdsManager::add_pfd(pollfd pfd){
    std::unique_lock<std::mutex> lock(pfd_mtx);
    auto it = closed_pfds.find(pfd); 
    if(it != closed_pfds.end()){
        closed_pfds.erase(it);
    }
    pfds.push_back(pfd);
}

void daq::felix2atlas::PfdsManager::add_pfd(pollfd pfd, std::string fifo){
    std::unique_lock<std::mutex> lock(pfd_mtx);
    pfds.push_back(pfd);
    fifo_by_fd[pfd.fd] = fifo;
}


std::vector<struct pollfd> daq::felix2atlas::PfdsManager::get_pfds(){
    std::unique_lock<std::mutex> lock(pfd_mtx);
    return pfds;
}

std::vector<struct pollfd> daq::felix2atlas::PfdsManager::get_closed_fds(){
    std::unique_lock<std::mutex> lock(pfd_mtx);
    std::vector<struct pollfd> v;
    v.assign(closed_pfds.begin(), closed_pfds.end());
    return v;
}

bool daq::felix2atlas::PfdsManager::closed_fifos_available(){
    std::unique_lock<std::mutex> lock(pfd_mtx);
    return closed_pfds.size();
 }

std::string daq::felix2atlas::PfdsManager::get_fifo_by_fd(int fd){
    if(fifo_by_fd.count(fd)){
        return fifo_by_fd[fd];
    } else {
        throw felix2atlas::FIFOIssue(ERS_HERE, std::string("No fifo associated to this FD: ") + std::to_string(fd));
    }
}
