#include <nlohmann/json.hpp>
#include "felix2atlas/ErsSink.hpp"
#include <ers/ers.h>
#include "felix2atlas/FelixERSreceiver.hpp"
#include <algorithm>


static const constexpr char DEBUG_LEVEL_LABEL[] = "level";
static const constexpr char MESSAGE_LABEL[] = "message";
static const constexpr char DEVICE_LABEL[] = "device";
static const constexpr char TIMESTAMP_LABEL[] = "timestamp";
static const constexpr char PACKAGE_LABEL[] = "package";
static const constexpr char FILENAME_LABEL[] = "filename";
static const constexpr char LINE_LABEL[] = "line";
static const constexpr char FUNCTION_LABEL[] = "functionname";
static const constexpr char HOST_LABEL[] = "hostname";
static const constexpr char PID_LABEL[] = "pid";
static const constexpr char TID_LABEL[] = "tid";
static const constexpr char CWD_LABEL[] = "cwd";
static const constexpr char UID_LABEL[] = "uid";
static const constexpr char UNAME_LABEL[] = "uname";
static const constexpr char APPNAME_LABEL[] = "appname";


daq::felix2atlas::ErsSink::ErsSink(){
    flxers = FelixERSreceiver();
}

void daq::felix2atlas::ErsSink::dispatch(const std::string &message) {
    try {
        nlohmann::json root = nlohmann::json::parse(message);
        //::ers::debug(felix2atlas::Felix2atlasIssue(ERS_HERE, "ERS MSG is: " + message));
        auto rpc = ers::RemoteProcessContext(root[HOST_LABEL].get<std::string>(),
                                             root[PID_LABEL].get<int>(),
                                             root[TID_LABEL].get<uint64_t>(),
                                             root[CWD_LABEL].get<std::string>(),
                                             root[UID_LABEL].get<int>(),
                                             root[UNAME_LABEL].get<std::string>(),
                                             root[APPNAME_LABEL].get<std::string>()); 

        ers::RemoteContext rc(root[PACKAGE_LABEL].get<std::string>(),
                              root[FILENAME_LABEL].get<std::string>(),
                              root[LINE_LABEL].get<int>(),
                              root[FUNCTION_LABEL].get<std::string>(),
                              rpc);

        flxers.sendERS("FelixIssue",
                        rc,
                        root[DEBUG_LEVEL_LABEL].get<std::string>(),
                        root[TIMESTAMP_LABEL].get<std::string>(),
                        root[MESSAGE_LABEL].get<std::string>(),
                        std::vector<std::string>(),
                        {{"device", root[DEVICE_LABEL].get<std::string>()}} );
    } catch (std::exception &ex) {
        ::ers::warning(felix2atlas::JsonIssue(ERS_HERE,"Unable to parse json message: " + message + " because " + ex.what()));
    }
}
