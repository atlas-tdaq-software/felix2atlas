#include <ostream>
#include "felix2atlas/IsSink.hpp"
#include "felix2atlas/FelixERSreceiver.hpp"
#include <ers/ers.h>


bool daq::felix2atlas::IsSink::sIsInitialized = false;

void daq::felix2atlas::IsSink::init(const std::list<std::pair<std::string, std::string>> &args) {
    IPCCore::init(args);
    sIsInitialized = true;
}

bool daq::felix2atlas::IsSink::isInitialized() {
    return sIsInitialized;
}

daq::felix2atlas::IsSink::IsSink(const std::string &partitionName, std::string server, bool historyMode) : mPartition(partitionName),
                                                                                              mServer(std::move(
                                                                                                      server)),
                                                                                              mDictionary(mPartition),
                                                                                              mHistoryMode(
                                                                                                      historyMode) {
    if (partitionName.empty() || mServer.empty()) {
        throw felix2atlas::Felix2atlasIssue(ERS_HERE, 
                "Invalid IS configuration! Partition: '" + partitionName + "' Server: '" + mServer + "'");
    }
    char hostname[1024];
    gethostname(hostname, 1024);
    mLocalhost = std::string(hostname);

    ::ers::debug(felix2atlas::Felix2atlasIssue(ERS_HERE, 
            "IS Partition: '" + partitionName + "'; server: '" + mServer + "'; history mode: " +
            std::to_string(mHistoryMode)));

}

void daq::felix2atlas::IsSink::dispatch(const std::string &message) {
    nlohmann::json root = nlohmann::json::parse(message);
    std::string nameSpace;
    std::string device = "N/A";

    if (root.find("device") != root.end()){
        device = root["device"].get<std::string>();
    }

    if (root.find("hostname") != root.end()){
        nameSpace = mServer + "." + root["hostname"].get<std::string>();
    }

    std::string app = root["app"].get<std::string>();
    nameSpace = nameSpace + ".d" + device;
    
    if (app == "tohost"){
        processToHostJson(root, nameSpace);
    } else if (app == "toflx"){
        processToFlxJson(root, nameSpace);
    } else {
        processJsonTree(root, nameSpace + ".general");
    }
}

void daq::felix2atlas::IsSink::processToHostJson(const nlohmann::json &root, const std::string &nameSpace) {
    if (root.size() > 0) {
        std::string globalName = nameSpace + ".tohost.global";
        //std::cout << "publishing at: " << globalName << std::endl;
        daq::felix2atlas::FELIX_globalNamed global (mPartition, globalName);
        initalGlobalStats(&global);
        for (auto itr = root.begin(); itr != root.end(); itr++) {
            if (itr.key() == "hostname"){global.hostname =  (*itr).get<std::string>();}
            else if (itr.key() == "ts"){
                char ts[20];
                strcpy(ts, (*itr).get<std::string>().c_str());
                global.ts = OWLTime(ts);
            }
            else if (itr.key() == "device"){global.device = (*itr).get<std::string>();}
            else if (itr.key() == "global"){processGlobalStats(*itr, &global);}
            else if (itr.key() == "stats"){processElinkJson(*itr, nameSpace );}            
        }
        global.checkin();    
    } else {
        ::ers::debug(felix2atlas::JsonIssue(ERS_HERE, std::string("Can't dispatch message to IS, message is empty.")));
    }
}

void daq::felix2atlas::IsSink::initalGlobalStats(daq::felix2atlas::FELIX_globalNamed* global){
        global->datarate = 0;
        global->blockrate= 0;
        global->chunkrate = 0;
        global->interrupts = 0;
        global->daq_co = 0;
        global->dcs_co = 0;
        global->ttc_co = 0;
        global->daq_pages = 0;
        global->dcs_pages = 0;
        global->ttc_pages = 0;
        global->buffer_events = 0;
        global->polls = 0;
        global->xl1id = 0;
        global->ecrs_from_ttc = 0;
        global->ttype_from_ttc = 0;
        global->bcr_mismatch = 0;
        global->bcr = 0;
        global->available_buf = 0;
        global->busy = 0;
}


void daq::felix2atlas::IsSink::processGlobalStats(const nlohmann::json &root, daq::felix2atlas::FELIX_globalNamed* global){
    for (auto itr = root.begin(); itr != root.end(); itr++) {
        if (itr.key() == "throughput[Gbps]"){global->datarate = (*itr).get<double>();}
        else if (itr.key() == "blockrate[kHz]"){global->blockrate= (*itr).get<double>();}
        else if (itr.key() == "chunkrate[kHz]"){global->chunkrate = (*itr).get<double>();}
        else if (itr.key() == "interrupts"){global->interrupts = (*itr).get<uint64_t>();}
        else if (itr.key() == "daq_co"){global->daq_co = (*itr).get<uint64_t>();}
        else if (itr.key() == "dcs_co"){global->dcs_co = (*itr).get<uint64_t>();}
        else if (itr.key() == "ttc_co"){global->ttc_co = (*itr).get<uint64_t>();}
        else if (itr.key() == "daq_pages"){global->daq_pages = (*itr).get<uint64_t>();}
        else if (itr.key() == "dcs_pages"){global->dcs_pages = (*itr).get<uint64_t>();}
        else if (itr.key() == "ttc_pages"){global->ttc_pages = (*itr).get<uint64_t>();}
        else if (itr.key() == "buffer_events"){global->buffer_events = (*itr).get<uint64_t>();}
        else if (itr.key() == "polls"){global->polls = (*itr).get<uint64_t>();}
        else if (itr.key() == "xl1id"){global->xl1id = (*itr).get<uint64_t>();}
        else if (itr.key() == "ecrs_from_ttc"){global->ecrs_from_ttc = (*itr).get<uint64_t>();}
        else if (itr.key() == "ttype_from_ttc"){global->ttype_from_ttc = (*itr).get<uint64_t>();}
        else if (itr.key() == "bcr_mismatch"){global->bcr_mismatch = (*itr).get<uint64_t>();}
        else if (itr.key() == "bcr"){global->bcr = (*itr).get<uint64_t>();}
        else if (itr.key() == "available_buf"){global->available_buf = (*itr).get<double>();}
        else if (itr.key() == "busy"){global->busy = (*itr).get<uint64_t>();}
        else {::ers::debug(felix2atlas::JsonIssue(ERS_HERE, std::string("Found unknown parameter: " + itr.key())));}
    }
}


void daq::felix2atlas::IsSink::processElinkJson(const nlohmann::json &root, const std::string &nameSpace){
    uint64_t count = 0;
    for (auto itr = root.begin(); itr != root.end(); itr++) {
        if(itr.key() != "elink"){
            std::string elinkName = nameSpace + "." + itr.key();//".elink[" + std::to_string(count) + "]";
            count++;
            daq::felix2atlas::ElinkNamed elink (mPartition, elinkName);
            elink.FID = itr.key();
            elink.chunks = (*itr)[0].get<uint64_t>();
            elink.rate = (*itr)[1].get<double>();
            elink.dropped  = (*itr)[2].get<uint64_t>();
            elink.largest_size = (*itr)[3].get<uint64_t>();
            elink.avg_size = (*itr)[4].get<double>();
            elink.fw_error = (*itr)[5].get<uint64_t>();
            elink.sw_error = (*itr)[6].get<uint64_t>();
            elink.fw_trunc = (*itr)[7].get<uint64_t>();
            elink.sw_trunc = (*itr)[8].get<uint64_t>();
            elink.fw_crc = (*itr)[9].get<uint64_t>();
            elink.checkin();
        }
    } 
}


void daq::felix2atlas::IsSink::processToFlxJson(const nlohmann::json &root, const std::string &nameSpace){
    std::string toFlxName = nameSpace + ".toflx.global";
    daq::felix2atlas::FELIX_toflxNamed toflx (mPartition, toFlxName);
    if (root.size() > 0) {
        for (auto itr = root.begin(); itr != root.end(); itr++) {
            if (itr.key() == "hostname"){toflx.hostname =  (*itr).get<std::string>();}
            else if (itr.key() == "ts"){
                char ts[20];
                strcpy(ts, (*itr).get<std::string>().c_str());
                toflx.ts = OWLTime(ts);
            }
            else if (itr.key() == "device"){toflx.device = (*itr).get<std::string>();}
            else if (itr.key() == "throughput[Gbps]"){toflx.throughput = (*itr).get<double>();}
            else if (itr.key() == "dma_rate[kHz]"){toflx.dma_rate = (*itr).get<double>();}
            else if (itr.key() == "msgs_received"){toflx.msgs_received = (*itr).get<uint64_t>();}
            else if (itr.key() == "msg_rate[kHz]"){toflx.msg_rate = (*itr).get<double>();}         
        }
        toflx.checkin();    
    } else {
        ::ers::warning(felix2atlas::JsonIssue(ERS_HERE, std::string("Can't dispatch message to IS, message is empty.")));
    }
}

void daq::felix2atlas::IsSink::processJsonTree(const nlohmann::json &root, const std::string &nameSpace) {
    if (root.size() > 0) {
        for (auto itr = root.begin(); itr != root.end(); itr++) {
            std::string newNameSpace = nameSpace;
            newNameSpace += '.';
            newNameSpace += itr.key();
            processJsonTree(*itr, newNameSpace);
        }
    } else {
        try {
            processJsonValue(root, nameSpace);
        }catch(std::exception &ex) {
            ::ers::error(felix2atlas::JsonIssue(ERS_HERE, std::string("Can't dispatch message to IS: ") + ex.what()));
        }
    }
}


void daq::felix2atlas::IsSink::processJsonValue(const nlohmann::json &root, const std::string &nameSpace) {
    if (root.is_number_integer()) {
        mDictionary.checkin(nameSpace, ISInfoLong(root.get<int64_t>()), mHistoryMode);
    } else if (root.is_number_float()) {
        mDictionary.checkin(nameSpace, ISInfoDouble(root.get<double>()), mHistoryMode);
    } else {
        mDictionary.checkin(nameSpace, ISInfoString(root.get<std::string>()), mHistoryMode);
    }
}
