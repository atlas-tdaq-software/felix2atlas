#pragma once

#include <fcntl.h> 
#include <poll.h> 
#include <ers/ers.h>
#include <nlohmann/json.hpp>
#include <mutex>
#include <atomic>
#include "SinkFactory.hpp"
#include "IsSink.hpp"
#include "Configuration.hpp"
#include "FelixERSreceiver.hpp"


namespace daq::felix2atlas {
    class PfdsManager {
        std::mutex pfd_mtx;
        std::vector<struct pollfd> pfds;
        std::set<struct pollfd> closed_pfds;
        std::map<int, std::string> fifo_by_fd;
    public: 
        void remove_pfd(pollfd pfd);
        void add_pfd(pollfd pfd);
        void add_pfd(pollfd pfd, std::string);
        std::vector<struct pollfd> get_pfds();
        std::vector<struct pollfd> get_closed_fds();
        bool closed_fifos_available();
        std::string get_fifo_by_fd(int fd);
    };

    class SubscribeClient {
    public:
        std::unique_ptr<daq::felix2atlas::PfdsManager> pfds_manager;
        std::map <std::string, daq::felix2atlas::Sink*> sinkByFifo;
        void on_init();
        void on_connect(uint64_t fid);
        void on_disconnect(uint64_t fid);
        void on_data(std::string fifo, const uint8_t* data, size_t size);
        virtual void connect(Configuration* config) {};
        std::vector<std::string> tokenize(std::string& rawdata, std::string& buffer);        
    };
    class SubscribeClientFIFO : public SubscribeClient {
    private:
        std::atomic<bool> fifos_configured = true;
        void read_fifo(const std::vector<std::string>& confs);
        void check_closed_fifos();

    public: 
        void connect(Configuration* config) override;
    };

}