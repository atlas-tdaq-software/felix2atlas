#pragma once

#include <string>
#include <list>
#include <utility>
#include <vector>
#include <unordered_map>


namespace daq::felix2atlas {
    struct Config {
        //std::string localHostName;
        std::vector<std::string> ersFifos; 
        std::vector<std::string> isFifos; 
        std::string server; 
        std::string mode; 
        std::string partition; 
    };


    class Configuration {
    public:
        class ChannelConfig {
        public:
            std::string mFifo; 

            struct SinkConfig {
                std::string mSinkType;
                std::unordered_map<std::string, std::string> mParams;
            } sinkConfig;

            ChannelConfig() = default;

            ChannelConfig(std::string fifo, std::string sinkType) : mFifo(fifo), sinkConfig({std::move(sinkType)}) {
            }

            std::string getFifo() const;

            const Configuration::ChannelConfig::SinkConfig &getSinkConfig() const;
        };

    private:
        std::vector<ChannelConfig> mConnectionConfigVector;
        //std::string mLocalHostname;
        std::list<std::pair<std::string, std::string>> mISInit;
        bool mISConfigPresent;
        // void extractIsArguments(struct Config* cfg);
        void extractConnections(struct Config* cfg);
    public:
        explicit Configuration(struct Config* cfg);

        const std::vector<ChannelConfig> &getChannelConfig() const;

        bool isISConfigPresent() const;

        const std::list<std::pair<std::string, std::string>> & getISInitList() const;

        virtual ~Configuration() = default;
    };
}

