#pragma once
#include <ers/ers.h>
#include <ers/Issue.h>
#include <ers/RemoteContext.h>

namespace daq::felix2atlas {
    class FelixERSreceiver {
        static ::ers::Severity severityFromString(const std::string &severityString);
        static system_clock::time_point timestampFromString(const std::string &timestampString);

    public:
        void sendERS(std::string msgID, const ers::RemoteContext &context, const std::string &severity,
                    const std::string &timestamp, const std::string &message, 
                    const std::vector<std::string>& qualifiers, const std::map<std::string, std::string>& parameters);

    };
    ERS_DECLARE_ISSUE(felix2atlas, Felix2atlasIssue, ERS_EMPTY, ERS_EMPTY)
    ERS_DECLARE_ISSUE(felix2atlas, JsonIssue, ERS_EMPTY, ERS_EMPTY)
    ERS_DECLARE_ISSUE(felix2atlas, FIFOIssue, ERS_EMPTY, ERS_EMPTY)
}
