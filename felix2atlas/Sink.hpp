#pragma once

#include <string>

namespace daq::felix2atlas {
    struct Sink {
        virtual void dispatch(const std::string &dataReceived) = 0;
        virtual ~Sink() = default;
        std::string BufferedJson = "";
    };
}
