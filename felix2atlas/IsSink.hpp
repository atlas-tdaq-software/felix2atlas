#pragma once

#include "Sink.hpp"
#include <is/infoT.h>
#include <is/infodictionary.h>
#include <ipc/core.h>
#include <nlohmann/json.hpp>
#include <felix2atlas_is_info/ElinkNamed.h>
#include <felix2atlas_is_info/FELIX_globalNamed.h>
#include <felix2atlas_is_info/FELIX_toflxNamed.h>

namespace daq::felix2atlas {
    class IsSink : public daq::felix2atlas::Sink {
        static bool sIsInitialized;

        IPCPartition mPartition;
        std::string mServer;
        std::string mLocalhost;
        ISInfoDictionary mDictionary;
        bool mHistoryMode;

        std::tm timestampFromString(const std::string &timestampString);
        void processJsonTree(const nlohmann::json &root, const std::string &nameSpace);
        void processJsonValue(const nlohmann::json&root, const std::string &nameSpace);
        void initalGlobalStats(daq::felix2atlas::FELIX_globalNamed* global);

    public:
        void processToHostJson(const nlohmann::json &root, const std::string &nameSpace);
        void processToFlxJson(const nlohmann::json &root, const std::string &nameSpace);
        void processGlobalStats(const nlohmann::json &root, daq::felix2atlas::FELIX_globalNamed* global);
        void processElinkJson(const nlohmann::json &root, const std::string &nameSpace);
        static void init(const std::list<std::pair<std::string, std::string>> & args);
        static bool isInitialized();
        IsSink(const std::string &partitionName, std::string server, bool historyMode);
        void dispatch(const std::string &message) override;
    };
}


